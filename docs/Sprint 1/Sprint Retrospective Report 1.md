# Sprint Retrospective Report 1

**Note:** Elaborate every answer in your own words, based on your team performance and experience. You are required to submit a pdf file (2-3 pages) for this assignment.
 
|                  | **Names/Ids**  |
|-----------------:|:---------------|
| *Team members:*  |Jean Louis, Fabian Niculae, Alex Hesson, Alex Isac, Floris Heinen, Matthijs Bonke   |
| *Team ID:*       |       53       |
| *Scrum  Master:* | Floris Heinen  |
| *Scrum  Mentor:* | Andrew  Heath, Chandni Raghuraman |
 

## *Q1:* What are the things that went well during the last sprint? 
        We feel like we have set realistic goals that we can actually achieve, leading to a unique and creative project, working well together to come up
        with what we think to be the most viable product. We noticed a high participation of each member in developing the new ideas for the product,
        everyone's strength played a big role in the future to be PiParty. We decided that each functionality, feature, attachment or idea to be voted by
        all the members of a team before the actual implementation for it, therefore we make sure the responsability is equally divded.
        
## *Q2:* What are the things that could have gone better during the last sprint?
        Participation at the meetings by all the members at the exact time, is something we should definitely improve. 
        We should also take a look at the productivity we have during our working sessions, by managing the tasks better.
        As a team, we felt like some degree of procrastination was visible during this last sprint, but we managed to cross
        the line at the same time with the other groups, by collective work. That is one thing we should definitely improve,
        by staying on track and follow the deadlines set.
        
## *Q3:* What did you learn during the last sprint on a….

### *a)* Domain specific level?
        From our research we concluded that our intended audience has a short attention span, so it made us focus on our UI implementation,
        to make it simple and friendly, while still having all the necessary displayed on the screen.
        
### *b)* intra-personal level?
        The product we intend to realise can be used both for entertainment purposes and educational.
        
## *Q4:* What are your plans for the next sprint?
        Building our prototype system is definitely something we will have our focus on, implementing the controllers software and handling the real-time 
        user interaction with the product. Also scalability of the system is our secondary focus, as we intend to grow the games library and supported 
        amount of players. Our objective is to already be able to synchronize the controllers and also measure the time that user took to press a button,
        after an event has occured (eg. the screen turned green). After all this is working properly, we plan on focusing our attention on fully creating
        and designing our first program for the platform. 
        
## *Q5:* Select your team performance and satisfaction score from the following. Don’t forget to justify your answer.

| **Satisfaction-level  and Value** | **Justification** |
| --------------------------------- | ----------------- |
| Very  Happy (5)                   |                   |
| Happy  (4)                        | Being that our idea is quite unique, and the fact that we found for the most part what our product requirments are, makes us a happy. |
| Okay  (3)                         |                   |
| Sad  (2)                          |                   |
| Very  Sad (1)                     |                   |

## *Q6:* Which team member was most appreciated during the last sprint (only list 1) and why?
        We like to think that everyone in the team is participating with ideas and skills to the development of the product, and we all equally share 
        the responsabilites and gains of the project. Although, we could highlight our team member Floris Heinen for having the initial idea that led 
        us as a team on the right track to shape our product.
## *Q7:*  Would you like to specify any other priority detail(s)/points which could be useful for the development of your product (if any)?
        Getting togheter physically and test the controllers with the main PI, this is impossible to do while remote.
        