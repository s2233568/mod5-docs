# Requirement Analysis Document Template
 
|                 | **Names/Ids**                                                                      |
|----------------:|:-----------------------------------------------------------------------------------|
| *Team members:* |  Alex Isac, Alex Hesson, Fabian Niculae, Floris Heinen, Jean Louis, Matthijs Bonke |
| *Team ID:*      |  Team 53                                                                           |


**Instructions:**
* Make a document by including all the suggested sections.
* The document should be of a maximum of 4 pages.

## Introduction
PiParty is an entertainment and educational system with various programs that can measure your reaction time, knowledge,
 short-term memory or simply play games or quizzes. It can be used for leisure activities among friends as well as for 
 educational purposes where you can measure statistics based on our products output. We make use of a Raspberry Pi to run 
 the core engine of our system, which interacts with 4 other controller raspberry pi’s at the same time, that 
 have 4 buttons and an RFID scanner for linking your game data with your profile.
 
### Motivation
PiParty is an entertainment and educational system with various programs that can measure your reaction time, knowledge, 
short-term memory or simply play games or quizzes. It can be used for leisure activities among friends as well as for 
educational purposes where you can measure statistics based on our products output. We make use of a Raspberry Pi to run 
the core engine of our system, which interacts with 4 other controller raspberry pi’s at the same time, that have 4 
buttons and an RFID scanner for linking your game data with your profile.

###  Scope
We aim to build this system for usage for all ages. Quizzes can be entertaining and educational for both young and old. 
Minigames are mostly focused towards the youth, and programs that test one’s memory and reaction time can be used to 
train young children and maintain skills for the elderly.

### Limitations of the current system (if any)
In the pre-development phase we can not find any limitations, as the system can grow exponentially based on the amount 
of work we invest in, having the possibility to add more more games and quizzes at any time.

### Objectives
PiParty’s objective is to have a user-friendly system running on the main Raspberry Pi with a web-server, displaying 
the available games and tests and the possibility to interact in real-time with the users over the internet using 
our own PiParty controllers.

### Definitions and Abbreviations 
PiParty: 
    The assembly of the MRP, Controllers and Programs.
Main Raspberry Pi (MRP): 
    This is the raspberry pi that hosts the web server that the controllers can connect to
Controllers: 
    These are the Raspberry Pi’s that have the buttons which the players can use to enter their answers to 
    the Main Raspberry Pi.
Web server (WS): 
    This is the web server, hosted by the Main Raspberry Pi, that displays the programs and collects the inputs of 
    the players via the Controllers
Program: 
    This can be any application that requires at least one Controller, like a quiz, game or Test.
Test: 
    This can be either a reaction test, memory test or knowledge test
Player: 
    User of the system that makes use of the Controllers to play the games and test its abilities.
Moderator (MOD): 
    The person who can manage users, quizzes, tests and others.

### Overview of the selected application
PiParty will be available at piparty.nl where the users can select which program they want to run, leading to a page 
where they are instructed to scan their Student Cards to one of the available RFID Scanners, and therefore joining 
the lobby of the running program. Once all the players have joined, the MOD can start the game. The user interaction 
is different for each program, but it is based on the Controllers, which are synchronized to the MRP so that input 
speeds are accurately measured. The Controllers will have 4 standard buttons of different colors as our main sensors 
and an RFID Scanner as a secondary sensor meant for authentication. The inputs of the sensors will be handled 
individually on each Controller using Python and sent via the internet to the MRP which will ultimately display and 
compare the answers. The MRP will also have buttons of its own, that can easily interact with the content 
displayed on the screen

## Product Requirements
### Functional requirements
-   The system should be able to link accounts to student cards
-	The system should be able to connect to the MRP after the user logged in with their student card
-	The system should be able to handle the user inputs when he presses the buttons for choosing the answer
-	The system should handle the user inputs for browsing between different games and  showing the user’s highest score for these games
-	The system should work smoothly and avoid crashes as much as possible.
-	The system should have all the quizzes and games in a working state.

### Nonfunctional requirements
-	Modified data on controllers should be updated on the MRP after 1 second and for all other controllers after 2 seconds.
-	Multiple controllers connected to the MRP does not affect the experience of the users
-	Controllers should be synced between them
-	After scanning the student card, the user will be automatically sent to the main screen where he can choose what game he wants to play
-	The controllers should send the user input to the web server by making use of its api

## Conclusion
PiParty is a real-time interactive system that supports multiple players to engage in a variety of programs and have 
fun, while also improving their skills and general knowledge, making use of Raspberry Pi, Apache Web server, Python 
and physical addons to the Raspberry Pi’s like other sensors.

## Reference
-
