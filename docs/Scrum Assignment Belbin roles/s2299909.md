# Assignment - Scrum

|                 | **Names/Ids**                |
|----------------:|:-----------------------------|
| *Student Name:  |  Floris Heinen               |
| *Team ID:*      |  53                          |
| *Email:*        |  f.heinen@student.utwente.nl |                      


**Note: This assignment is to judge your scrum skills and how well you contribute to a team. You have to complete this form every team member individually.** 

## Q1: What role do you prefer for yourself as a team player and why?

	My preferred belbin role would be the specialist or the complete finisher. The former because
	I am often very curious about how things work, and I would like to know everything about a certain aspect.
	The latter because I tend to be a perfectionist with a great eye for detail, which suits
	the complete finisher role.

## Q2: How much productive you are? Can you give an estimation of your productivity? How do you manage in case your estimation goes wrong?

	I would say that I sometimes spend the most time on a project, while not being the most productive,
	as I tend to care a lot about the quality of my work. I could make a certain part almost perfect,
	while a much lower quality suffices.
	
## Q3: How do you prioritize your task? Do you have any specific criteria which you follow every time?

	I like to prioritise fun taks over boring tasks. If this prioritisation is not possible, I usually
	pick the task that I know I can do best or I will pick the task that is the most urgent.

## Q4: How do you follow the progress of your project?

	I follow my progress by the amount of functionality of the product. I do not really think about
	how much time I have left to finish something or what my progress has been since I started, 
	as I usually just keep working untill it is finished.
	
## Q5: How do you interact in a meeting? What time generally you spend during meetings? Would you like to give any feedback to have better meetings?

	Most of the time I am in a meeting, I am listening to what others have to say. And if there
	is something I would like to add, mention or disagree with, I would tell my team.

## Q6: How do you decide the requirements of your application? What are your criteria to decide if the product is genuinely completed?

	This depends on the project, as I tend to have two different approaches to finalising a project.
	Sometimes I think the project suffices if it has all the necessary functionality, other times
	I will only settle when there is nothing more I could do to increase the quality of the product.

## Q7: Do you think the evaluation is important for productivity? How do you assess yourself and your Colleagues? 

	The evaluation should bring up the things that went well and the things that could get more attention.
	The evaluation on things that did not go well are for me an extra motivation to to better next time.
	I assess myself and my teammates not on their productivity, but on their effort and the amount of time
	they spent on the project. I understand that one person is more efficient in or better at a certain thing,
	so I think it would be unfair to asses people by the amount of work they did.