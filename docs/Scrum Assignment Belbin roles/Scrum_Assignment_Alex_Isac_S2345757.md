# Assignment - Scrum

|                 | **Names/Ids**  |
|----------------:|:---------------|
| *Student Name:* |   Alex Isac    |
| *Team ID:*      |      53        |
| *Email:*        |     a.isac@student.utwente.nl     |                      


## *Q1:* What role do you prefer for yourself as a team player and why?
        The belbin role I find to suit me best would be the Implementor and the Plant. Past experience and projects give me a boost when contributing to the development of a new product, I like working hard for a project and therefore the Implementor role. I like to brainstorm creative and new concepts for the implementation of my projects, ways of doing that others won't think of so the Plant role.
## *Q2:* How much productive you are? Can you give an estimation of your productivity? How do you manage in case your estimation goes wrong?
        I enjoy working in long sessions for a project, seeing how the project grows step by step, and I find this method to be the one that gives me the most productivity. There is always some loss from me when spend time on adding lots of new functionalities that probably don't even matter that much to the tasks I have, but do for me. 
## *Q3:* How do you prioritize your task? Do you have any specific criteria which you follow every time?
        When dealing with a larger project, I first order all the big tasks based on deadlines, then dividing all those tasks into lots of small tasks and set short deadlines for each of them, one after the other.
## *Q4:* How do you follow the progress of your project?
        I keep personal notes for every version release, after inspecting the product carefuly each time.
## *Q5:* How do you interact in a meeting? What time generally you spend during meetings? Would you like to give any feedback to have better meetings?
        During meetings, sometimes I tend to take over the talk, but recently I am focusing on being more concise and short, only being an active participant for the matters that involve me, giving the others the opportunity to speak their part. 
## *Q6:* How do you decide the requirements of your application? What are your criteria to decide if the product is genuinely completed?
        Most of the times, the product has to evolve along whith time, so there will always something new or better to add or improve. Although, I agree with calling a product completed once all the requirements are met, all the known bugs fixed and enough good feedback received.
## *Q7:* Do you think the evaluation is important for productivity? How do you assess yourself and your Colleagues?
        Keeping track of everyones progress, time, effort and passion in a team is an essential tool for me and it is always in our best interest to find out the outcomes of our work. I find Evaluation both motivating and discouraging sometimes, as it plays a big role in the balance of a project.