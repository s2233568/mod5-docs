# Assignment - Scrum

|                 | **Names/Ids**             |
|----------------:|:--------------------------|
| *Student Name:  |Alexandru Hesson           |
| *Team ID:*      |team 53                    |
| *Email:*        |a.hesson@student.utwente.nl|                      


**Note: This assignment is to judge your scrum skills and how well you contribute to a team. You have to complete this form every team member individually.** 

## Q1: What role do you prefer for yourself as a team player and why?
I prefer the role of Teamworker as a team player because I feel that I can always help the team glue together due to my diplomatic skills and I can help with diminuating tense situation related to taking big project decisions.

## Q2: How much productive you are? Can you give an estimation of your productivity? How do you manage in case your estimation goes wrong?
I believe I am a fairly productive person, and I can usually estimate my productivity pretty well, but in case it goes wrong, I try to lift them back to my initial estimate.

## Q3: How do you prioritize your task? Do you have any specific criteria which you follow every time?
I prioritize my tasks by levels, most important tasks get done first, then least important second.

## Q4: How do you follow the progress of your project?
By talking with my team mates, and scrum meetings also help. 

## Q5: How do you interact in a meeting? What time generally you spend during meetings? Would you like to give any feedback to have better meetings?
I try to keep my srum meetings short, I give the necesary information and no bloat, because nobody wants to hear a TED talk during a meeting. I interact with people by answering their questions if they have any. As a feedback for better meetings I would say to people to try and keep their speech as short as possible, but to retain all the necesary information. 

## Q6: How do you decide the requirements of your application? What are your criteria to decide if the product is genuinely completed?
Usually the requirements are handed in during the assingment, if not I like to talk with my team to see what requirements should we decide on doing. Only if those criteria are met can we reease the product as complete.

## Q7: Do you think the evaluation is important for productivity? How do you assess yourself and your Colleagues? 
Yes, I believe evaluation is important for productivity because it tells you if you did enough over a time span. I asses myself my the goals I told myself I would do, and if I complete them it means I did good productivity wise. I assess my colleagues in the same way. 