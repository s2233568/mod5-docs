# Assignment - Scrum

|                 | **Names/Ids**  |
|----------------:|:---------------|
| *Student Name:  |   Jean Louis   |
| *Team ID:*      |        53      |
| *Email:*        | j.l.clemente@student.utwente.nl |                      


**Note: This assignment is to judge your scrum skills and how well you contribute to a team. You have to complete this form every team member individually.** 

## Q1: What role do you prefer for yourself as a  team player and why?
I think for myself i would prfer to have a thinking-oriented role as i feel that i am realtively good at solving problems and coming up with interesting ideas and therfeore the role of "Plant" would best fit me.
However, i beleive that i can also be very logical when it comes to large team decisions and could possibly also fit into that role.

## Q2: How much productive you are? Can you give an estimation of your productivity? How do you manage in case your estimation goes wrong?
I would say that i am a productive person even though at times i might take things a little easy, but when the work must be done i always insure that i complete my work.
If things start to go wrong I normally ask some colleagues for a little bit of help and make anote of the situatuion so if i have a similar task the next time around i prepare my self better.

## Q3: How do you prioritize your task? Do you have any specific criteria which you follow every time?
I categorize my tasks in two categories, one is the level of the task and the other is the importance of the the task. High level and very important tasks get completed first,
 followed Medium level and very important tasks and so on and so forth.

## Q4: How do you follow the progress of your project?
I talk to my team mates, attend the scrum mettings, follow the scrum board and even look at the commit history on gitlab to get an idea of how fast the project is moving,
 and what needs to planned for the future to continue a smooth work flow.

## Q5: How do you interact in a meeting? What time generally you spend during meetings? Would you like to give any feedback to have better meetings?
In a metting i generally try to state what i have completed, even though this might seem redundant it is nice for others to know your progress so if your going off track then they can help or guide you.
We spend sometime 5 to 10 mins per scrum meeting. I think one suggestion would be at the end of the scrum metting to potentailly discuss as a group on how to move forward the project.


## Q6: How do you decide the requirements of your application? What are your criteria to decide if the product is genuinely completed?
For me the best way to decide the requirments of the project is to step in the shoes of a potentialy user of the product and to think about what features would be usefull to have.
Once again to decide if the producted is completed i would find a potential user and have them give us feedback on the product and maybe even discuss about how to further improve the product.

## Q7: Do you think the evaluation is important for productivity? How do you assess yourself and your Colleagues? 
I think the evaluation is very important as it provides us with time to refelcet and what we have done and how we can improve on future projects.
And as a result it can serve as a guide for working better and more effectively, thus improving productivity.
When it comes to assessing my colleagues i have to keep an open mind and take off any biases i may have towards them and provide them with a very frank evaluation.
