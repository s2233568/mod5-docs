# Assignment - Scrum
|                 | **Names/Ids**  |
|----------------:|:---------------|
| *Student Name:  | Matthijs Bonke |
| *Team ID:*      | 53             |
| *Email:*        | m.s.bonke@student.utwente.nl|                     
**Note: This assignment is to judge your scrum skills and how well you contribute
to a team. You have to complete this form every team member individually.**
## Q1: What role do you prefer for yourself as a team player and why?
I would describe myself as thinker, although I can also be a worker
Sometimes. I am the person that likes to come up with solutions to a problem
Or ideas for the project. However i don't like to be in the spotlight and have
All the responsibility of a team. Therefore I would like a role that gives the
Team ideas and solutions to problems but not a role that organizes the team. 
the role that would fit, would be the product owner. Or someone related to the
Product owner.
  
## Q2: How much productive you are? Can you give an estimation of your
productivity? How do you manage in case your estimation goes wrong?
If I am really focussed, I can really get some work done. However, there are 
Times when I get distracted. Reasons being; the subject is totally not in my 
Area of interest or I am stuck for too long. An other thing, It is hard for me
To get started working on work. However, when finally started I can be quite
Productive for a long period of time.

## Q3: How do you prioritize your task? Do you have any specific criteria which you
follow every time?
The Tasks with the shortest deadline would be the first to get started on.
If Tasks have a similar deadline, I would go with the most fun one out of the 
Two. Even Though the less fun task could have a deadline a day earlier. It could also
depend on the mood that I am in. If I been working on a task for days, I will 
Switch to another task.

## Q4: How do you follow the progress of your project?
I would not say that I follow the progress of the project by the minute. Rather by
Day. I really helps to have a discord server where we post information about the
Project (almost) every day. 

## Q5: How do you interact in a meeting? What time generally you spend during
meetings? Would you like to give any feedback to have better meetings?
I would say I act more as a support person. Not being in the spotlight, however if I
Got something relevant to say I won’t stay quiet. I am definitely not the person to 
Lead a meeting. Our meetings are of various lengths, some are short if we have
Something small to talk about, most of our meetings are like this. However for
Discussions, they are quite long. Having Discord is really convenient if you just
Small question and if it turns out that it is not a small question, calling someone
Is a piece of cake.

## Q6: How do you decide the requirements of your application? What are your
criteria to decide if the product is genuinely completed?
I would decide the requirements of our product based on the end goal of the project.
Choosing the bare minimum first and add fun features on top of that. The product is
Finished if all the bare minimum requirements are met, however if we are not totally
Happy with the bare minimum. I would say that if at least half the fun extra features
Are done, the product could be called complete.

## Q7: Do you think the evaluation is important for productivity? How do you assess
yourself and your Colleagues?
Yes, I think evaluation of work is important for the productivity of the team.
If people get stuck on something, with evaluation of work the others could help this
Person to get unstuck and find a solution. Also the others could encourage the person
To keep going, what leads to a boost in productivity. I would asses others and myself
Based on what they said they were going to deliver and what they really delivered.
