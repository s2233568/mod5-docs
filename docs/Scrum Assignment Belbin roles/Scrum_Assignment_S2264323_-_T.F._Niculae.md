# Assignment - Scrum

|                 | **Names/Ids**  |
|----------------:|:---------------|
| *Student Name:  |Theodor-Fabian Niculae|
| *Team ID:*      |    Team 53     |
| *Email:*        |t.f.niculae@student.utwente.nl|                      


**Note: This assignment is to judge your scrum skills and how well you contribute to a team. You have to complete this form every team member individually.** 

## Q1: What role do you prefer for yourself as a team player and why?
		I would prefer the role of a shaper because I have the capabilities and the strenght to make me and my team members work as hard as we can to achieve
		the desired product. I like to talk to others, to make them work together efficiently and to survive difficult situations. 

## Q2: How much productive you are? Can you give an estimation of your productivity? How do you manage in case your estimation goes wrong?
		I am efficient when it comes to productivity. I make small breaks between working hours and that helps me relax and continue to start working again and again.
		As a estimation I usually finish 90% of what I want for that day. I usually stay after the program to finish everything before I start to work next day.
## Q3: How do you prioritize your task? Do you have any specific criteria which you follow every time?
		I take in consideration the deadline and the weight of the task. I start with the most important and hardest task.
## Q4: How do you follow the progress of your project?
		I check the progress every day and if something is not right I try to make everyone in the team to work at it and don't put off for tomorrow what they can do today.

## Q5: How do you interact in a meeting? What time generally you spend during meetings? Would you like to give any feedback to have better meetings?
		I spend a lot of time in meetings if we work together or if we just talk. I usually talk after I heard all the opinions of the team members.
## Q6: How do you decide the requirements of your application? What are your criteria to decide if the product is genuinely completed?
		I decide the requirements based on how much time we have and the complexity of the task. It is finished when we have finalized the "To Do" list.If we have time we can add further
		functionalities to the project.
## Q7: Do you think the evaluation is important for productivity? How do you assess yourself and your Colleagues? 
		Evaluation is very important because we can see how much we've completed so far in the project and what steps should we start making for further refinements.
		Everyone should know how to interact with other team members.