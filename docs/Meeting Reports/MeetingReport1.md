# Project Standup Meeting Report (Template)

|                          | **Names/Ids**  |
|-------------------------:|:---------------|
| *Project Name:*          |  ?             |
| *Meeting Date:*          |  14 09 2020    |
| *Present Team members:*  |  Alex Hesson, Alex Isac, Fabian, Floris, Jean, Matthijs              |
| *Team ID:*               |  53              |
| *Scrum  Master:*         |  Floris              |
| *Scrum  Mentor:*         |  Andrew Heath and Chandni Raghuraman              |
 
## Meeting questions

**Example questions: What did you finish yesterday?, What will you do today?, Any obstacles?   Answers: Yesterday: ..., Today: ..., Obstacle (if any)?**

| **Team Members' Name (Student ID)** | **Meeting Questions** | **Response**  |
|-------------------------------------|--------------------------------------|----------------------------------------------------------------------------------|
| Floris                              |  What have we done till now?         |  Not much, Alex Isac has looked in the project guide                             |
| Floris                              |  What will we do after this meeting? |  Alex Isac will create a git repository and the rest will think of project ideas |
| Floris                              |  Are there any questions left?       |  Nope                                                                            |