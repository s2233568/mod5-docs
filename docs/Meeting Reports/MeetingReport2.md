# Project Standup Meeting Report (Template)

|                          | **Names/Ids**                                           |
|-------------------------:|:--------------------------------------------------------|
| *Project Name:*          |  ?                                                      |
| *Meeting Date:*          |  18 09 2020                                             |
| *Present Team members:*  |  Alex Hesson, Alex Isac, Fabian, Floris, Jean, Matthijs |
| *Team ID:*               |  53                                                     |
| *Scrum  Master:*         |  Floris                                                 |
| *Scrum  Mentor:*         |  Andrew Heath and Chandni Raghuraman                    |
 
## Meeting questions

**Example questions: What did you finish yesterday?, What will you do today?, Any obstacles?   Answers: Yesterday: ..., Today: ..., Obstacle (if any)?**

| **Team Members' Name (Student ID)** | **Meeting Questions**                 | **Response**                                                                             |
|-------------------------------------|---------------------------------------|------------------------------------------------------------------------------------------|
| Floris                              |  What have we done till now?          |  We finished our first assignment about our belbin roles and we thought of project ideas |
| Floris                              |  What will we do after this meeting?  |  We will meet again to make a choice about which project we will be making.              |
| Floris                              |  Are there any questions left?        |  Nope                                                                                    |